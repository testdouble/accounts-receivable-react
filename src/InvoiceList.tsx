import { Link, useLocation } from "react-router";
import { useInvoices } from "./store";

function InvoiceList() {
  const location = useLocation();
  const { getInvoices } = useInvoices();
  const invoices = getInvoices();

  return (
    <ul id="invoice-list" className="flex flex-col space-y-1">
      {invoices.map((invoice) => (
        <li key={invoice.id}>
          <Link
            to={`/invoices/${invoice.id}`}
            className={`block px-4 py-2 rounded transition ${
              location.pathname === `/invoices/${invoice.id}`
                ? "bg-blue-600 text-white"
                : "text-blue-600 hover:text-underline"
            }`}
          >
            {invoice.client}
          </Link>
        </li>
      ))}
    </ul>
  );
}

export default InvoiceList;
