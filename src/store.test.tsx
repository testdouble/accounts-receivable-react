import { describe, it, expect } from 'vitest';
import { Invoice, InvoiceState, invoiceReducer } from './store';

describe('invoiceReducer', () => {
  it('should prevent duplicate seeding of data', () => {
    const initialState: InvoiceState = { invoices: {}, seeded: false };
    
    const stateAfterFirstSeed = invoiceReducer(initialState, { type: 'SEED_DATA' });
    expect(stateAfterFirstSeed.seeded).toBe(true);
    expect(Object.keys(stateAfterFirstSeed.invoices).length).toBeGreaterThan(0);
    
    const stateAfterSecondSeed = invoiceReducer(stateAfterFirstSeed, { type: 'SEED_DATA' });
    expect(stateAfterSecondSeed).toBe(stateAfterFirstSeed);
  });

  it('should handle adding a new invoice while maintaining existing state', () => {
    const existingInvoice: Invoice = {
      id: '1',
      client: 'Existing Client',
      attn: 'Someone',
      due_date: '2024-03-01',
      notes: '',
      projects: [],
      services: [],
      created_at: '2024-02-01'
    };

    const initialState: InvoiceState = {
      invoices: { '1': existingInvoice },
      seeded: true
    };

    const newInvoice: Invoice = {
      id: '2',
      client: 'New Client',
      attn: 'Someone Else',
      due_date: '2024-03-15',
      notes: '',
      projects: [],
      services: [],
      created_at: '2024-02-15'
    };

    const newState = invoiceReducer(initialState, { type: 'ADD_INVOICE', payload: newInvoice });
    
    expect(Object.keys(newState.invoices)).toHaveLength(2);
    expect(newState.invoices['1']).toBe(existingInvoice);
    expect(newState.invoices['2']).toBe(newInvoice);
  });
});
