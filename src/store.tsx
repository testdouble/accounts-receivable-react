import { createContext, useContext, useReducer, ReactNode, ReactElement, Dispatch, useEffect } from 'react';
import seedData from './seed.json';

export interface LineItem {
  name: string;
  description: string;
  price_cents: number;
  discount_percent: number | null;
  quantity: number;
}

export interface Invoice {
  id: string;
  client: string;
  attn: string;
  due_date: string;
  notes: string;
  projects: LineItem[];
  services: LineItem[];
  created_at: string;
}

export interface InvoiceState {
  invoices: Record<string, Invoice>;
  seeded: boolean;
}

type InvoiceAction = 
  | { type: 'ADD_INVOICE'; payload: Invoice }
  | { type: 'SEED_DATA' };

const initialState: InvoiceState = {
  invoices: {},
  seeded: false,
};

export const invoiceReducer = (state: InvoiceState = initialState, action: InvoiceAction): InvoiceState => {
  switch (action.type) {
    case 'ADD_INVOICE':
      return {
        ...state,
        invoices: {
          ...state.invoices,
          [action.payload.id]: action.payload,
        },
      };
    case 'SEED_DATA':
      if (state.seeded) return state;
      return {
        seeded: true,
        invoices: (seedData.invoices as Invoice[]).reduce((acc, invoice) => {
          acc[invoice.id] = {
            ...invoice,
            created_at: invoice.created_at,
          };
          return acc;
        }, {} as Record<string, Invoice>),
      };
    default:
      return state;
  }
}

interface InvoiceContextType {
  state: InvoiceState;
  dispatch: Dispatch<InvoiceAction>;
  addInvoice: (invoice: Omit<Invoice, 'id' | 'created_at'>) => Invoice;
  getInvoices: () => Invoice[];
  getInvoice: (id: string) => Invoice | undefined;
}

const InvoiceContext = createContext<InvoiceContextType | null>(null);

export function InvoiceProvider({ children }: { children: ReactNode }): ReactElement {
  const [state, dispatch] = useReducer(invoiceReducer, initialState);

  const addInvoice = (invoice: Omit<Invoice, 'id' | 'created_at'>) => {
    const id = crypto.randomUUID();
    const newInvoice = {
      ...invoice,
      id,
      created_at: new Date().toISOString(),
    };
    dispatch({ type: 'ADD_INVOICE', payload: newInvoice });
    return newInvoice;
  };

  const getInvoices = () => Object.values(state.invoices);
  
  const getInvoice = (id: string) => state.invoices[id];

  const value = {
    state,
    dispatch,
    addInvoice,
    getInvoices,
    getInvoice
  };

  return (
    <InvoiceContext.Provider value={value}>
      {children}
    </InvoiceContext.Provider>
  );
}

export function useInvoices(): InvoiceContextType {
  const context = useContext(InvoiceContext);
  if (!context) {
    throw new Error('useInvoices must be used within an InvoiceProvider');
  }
  return context;
}

export function useSeedData(): void {
  const { dispatch } = useInvoices();
  useEffect(() => {
    dispatch({ type: 'SEED_DATA' });
  }, [dispatch]);
}
