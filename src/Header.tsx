import { NavLink } from "react-router";

export default function Header() {
  return (
    <header className="bg-white shadow">
      <div className="container mx-auto px-4">
        <div className="flex justify-between items-center py-6">
          <div>
            <h1 className="text-2xl font-bold text-gray-900">Invoice Time</h1>
            <p className="text-sm text-gray-500">a project by test double</p>
          </div>
          <NavLink
            className="inline-block bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600 transition"
            to="/invoices/add"
          >
            Add New Invoice
          </NavLink>
        </div>
      </div>
    </header>
  );
}
