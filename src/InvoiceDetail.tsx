import { useParams } from "react-router";
import { useInvoices } from "./store";
import type { LineItem } from "./store";

const formatCurrency = (cents: number) => {
  return `$ ${cents / 100}`;
};

const LineItemTable = ({ items }: { items: LineItem[] }) => (
  <div className="overflow-x-auto">
    <table className="min-w-full divide-y divide-gray-200">
      <thead>
        <tr>
          <th className="px-4 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
          <th className="px-4 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
            Description
          </th>
          <th className="px-4 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">
            Unit Price
          </th>
          <th className="px-4 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">Quantity</th>
          <th className="px-4 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">Subtotal</th>
        </tr>
      </thead>
      <tbody className="divide-y divide-gray-200">
        {items.map((item, index) => (
          <tr key={index} className="hover:bg-gray-50">
            <td className="px-4 py-3 text-sm text-gray-900">{item.name}</td>
            <td className="px-4 py-3 text-sm text-gray-500">{item.description}</td>
            <td className="px-4 py-3 text-sm text-gray-900 text-right">{formatCurrency(item.price_cents)}</td>
            <td className="px-4 py-3 text-sm text-gray-900 text-right">{item.quantity}</td>
            <td className="px-4 py-3 text-sm text-gray-900 font-medium text-right">
              {formatCurrency(item.price_cents * item.quantity)}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
);

const InvoiceDetail = () => {
  const { id } = useParams();
  const { getInvoice } = useInvoices();
  const invoice = getInvoice(id || "");

  if (!invoice) {
    return <div className="text-gray-500">Select an invoice to view details</div>;
  }

  return (
    <div className="bg-white shadow-sm rounded-lg p-6">
      <h2 className="text-2xl font-bold text-gray-900 mb-4">{invoice.client} Invoice</h2>

      <div className="space-y-2 mb-6">
        <div>
          <span className="font-medium text-gray-700">Attn: </span>
          <span className="text-gray-600">{invoice.attn}</span>
        </div>
        <div>
          <span className="font-medium text-gray-700">Due Date: </span>
          <span className="text-gray-600">{invoice.due_date}</span>
        </div>
        <div>
          <span className="font-medium text-gray-700">Notes: </span>
          <span className="text-gray-600">{invoice.notes}</span>
        </div>
      </div>

      <div className="space-y-6">
        <div className="projects-section">
          <h3 className="text-lg font-semibold text-gray-900 mb-3">Projects</h3>
          <LineItemTable items={invoice.projects} />
        </div>

        <div className="services-section">
          <h3 className="text-lg font-semibold text-gray-900 mb-3">Services</h3>
          <LineItemTable items={invoice.services} />
        </div>

        <div className="total-section">
          <h3 className="text-lg text-gray-900 mb-3"><span className="font-semibold">Total: </span>$1,000,000</h3>
        </div>
      </div>
    </div>
  );
};

export default InvoiceDetail;
