import React from "react";
import { Link, useLocation } from "react-router";

function Breadcrumbs() {
  const location = useLocation();

  const pathSegments = location.pathname.split("/").filter(Boolean);

  const crumbs = pathSegments.map((segment, index) => {
    const path = `/${pathSegments.slice(0, index + 1).join("/")}`;
    return {
      path,
      label: segment.charAt(0) + segment.slice(1),
    };
  });

  return (
    <div className="bg-gray-100 border-gray-200 border-b border-t">
      <div className="container mx-auto px-4">
        <nav className="py-3 text-sm" aria-label="Breadcrumb">
          <ol id="breadcrumbs" className="flex items-center space-x-2">
            <li>
              <Link to="/" className="text-gray-500 hover:text-gray-700">
                Home
              </Link>
            </li>
            <li className="text-gray-400">/</li>
            <li>
              <Link to="/invoices" className="text-gray-500 hover:text-gray-700">
                Invoices
              </Link>
            </li>
            {crumbs.map(
              (crumb) =>
                crumb.label.toLowerCase() !== "invoices" && (
                  <React.Fragment key={crumb.path}>
                    <li className="text-gray-400">/</li>
                    <li className="text-gray-700">{crumb.label}</li>
                  </React.Fragment>
                ),
            )}
          </ol>
        </nav>
      </div>
    </div>
  );
}

export default Breadcrumbs;
