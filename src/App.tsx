import { BrowserRouter, Routes, Route } from "react-router";

import Header from "./Header";
import Breadcrumbs from "./Breadcrumbs";
import InvoiceList from "./InvoiceList";
import InvoiceDetail from "./InvoiceDetail";
import InvoiceAdd from "./InvoiceAdd";
import { InvoiceProvider, useSeedData } from "./store";

const InvoiceLayout = () => (
  <div className="flex flex-col md:flex-row gap-8">
    <div className="w-full md:w-1/5">
      <InvoiceList />
    </div>
    <div className="w-full md:w-4/5">
      <Routes>
        <Route path="/invoices/:id" element={<InvoiceDetail />} />
        <Route path="/" element={<InvoiceDetail />} />
      </Routes>
    </div>
  </div>
);

function AppContent() {
  useSeedData();

  return (
    <BrowserRouter>
      <Header />
      <Breadcrumbs />
      <main className="container mx-auto px-4 py-8">
        <Routes>
          <Route path="/invoices/add" element={<InvoiceAdd />} />
          <Route path="/*" element={<InvoiceLayout />} />
        </Routes>
      </main>
    </BrowserRouter>
  );
}

function App() {
  return (
    <InvoiceProvider>
      <AppContent />
    </InvoiceProvider>
  );
}

export default App;
