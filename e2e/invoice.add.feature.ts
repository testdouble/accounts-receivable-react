import { test } from '@playwright/test';

test('test', async ({ page }) => {
    await page.goto('http://localhost:5173/');
    await page.getByRole('link', { name: 'Add New Invoice' }).click();
    await page.getByLabel('Client').selectOption('CircleCi');
    await page.getByLabel('Attention To').fill('Ralph Wiggum');
    await page.getByLabel('Due Date').fill('2026-12-13');
    await page.getByLabel('Notes').click();
    await page.getByLabel('Notes').fill('Takes payment in donuts! ');
    await page.getByRole('button', { name: 'Add Invoice' }).click();
    await page.getByText('Takes payment in donuts!').isVisible();
});