import { defineConfig, devices } from '@playwright/test';
import path from 'path';
import { fileURLToPath } from 'url';

const __dirname = path.dirname(fileURLToPath(import.meta.url));
const PORT = 5173;
const baseURL = `http://localhost:${PORT}`;

export default defineConfig({
    testDir: path.join(__dirname, 'e2e'),
    timeout: 70 * 1000,
    fullyParallel: false,
    forbidOnly: !!process.env.CI,
    retries: 0,
    workers: process.env.CI ? 1 : 2,
    reporter: [['list']],
    use: {
        baseURL,
        trace: 'retain-on-first-failure',
    },
    projects: [
        {
            name: 'features',
            testMatch: /.*\.feature\.ts/,
            use: {
                ...devices['Desktop Chrome'],
            }
        }
    ],

    webServer: {
        command: 'npm run dev',
        port: PORT,
        timeout: 60 * 1000,
        reuseExistingServer: !process.env.CI,
        stdout: 'pipe',
    }
});