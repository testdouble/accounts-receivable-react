This project was scaffolded using `npm create vite@latest accounts-receivable-react -- --template react-ts`

## Accounts Receivable App

An application for managing invoices.

The following tools and libraries are used in the project:

1. [React](https://react.dev)
1. [TailwindCSS](https://www.tailwindcss.com)
1. [React Router](https://reactrouter.com)
1. [Vitest](https://vitest.dev)
1. [Playwright](https://playwright.dev)

## Prerequisites

You will need to [install Node.js](https://nodejs.org/en/download/package-manager) to run this project.

## Available Scripts

In the project directory, you can run:

### `npm run dev`

Runs the app in the development mode.<br>
Open [http://localhost:5173](http://localhost:5173) to view it in the browser.

### `npm test`

Runs all test files (`.test.tsx`, `.test.ts`) in the project.<br>

### `npm run test:e2e`

Runs the end-to-end tests using [Playwright](https://playwright.dev).<br>

## Project Todos

Please perform your work as though you were delivering it to a paying client and
deploying it into production. We care more about the quality of your solution
than necessarily how far you get.

> [!NOTE]
>
> If you receive alternative instructions from a Test Double Agent for this project, then stick to what they said.

Complete the following tasks:

<details>
<summary>1: Compute Total</summary>

Use the price and quantity in the payload to calculate the invoice total. 
Currently invoice total is static to $1,000,000 for all invoices.

Example below:
```
+-------------------------------------------------------------------------------+
| Invoice Time                                                                  |
| a project by test double                                   [Add New Invoice]  |
+-------------------------------------------------------------------------------+
| Home / Invoices / 6ba7b810-9dad-11d1-80b4-00c04fd430c8                        |
+-------------------------------------------------------------------------------+
|                                                                               |
| +------------+  +-----------------------------------------------------------+ |
| | CircleCi   |  |                   New Relic Invoice                       | |
| |            |  |                                                           | |
| | New Relic  |  | Attn: Matan <Matan@new-relic.com>                         | |
| |            |  | Due Date: 10/20/2020                                      | |
| |            |  | Notes: Send weekly invoices.                              | |
| |            |  |                                                           | |
| |            |  | Projects                                                  | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  | | Name          Description         Price    Qty  Total | | |
| |            |  | | Video Software Subscription...    $140      1   $140  | | |
| |            |  | | Heroku        Monthly server...   $280      1   $280  | | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  |                                                           | |
| |            |  | Services                                                  | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  | | Name              Description     Price    Qty  Total | | |
| |            |  | | Software Dev.     Refactor...     $150      8   $1200 | | |
| |            |  | | Software Dev.     Extract...      $150     12   $1800 | | |
| |            |  | | Software Dev.     Deploy...       $150      9   $1350 | | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  |                                                           | |
| |            |  | Total: $1,000,000                                         | |
| |            |  |                                                           | |
| +------------+  +-----------------------------------------------------------+ |
|                                                                               |
+-------------------------------------------------------------------------------+
```
</details>

<details>
<summary>2: Compute Invoice Discount</summary>

Use the `discount_percent` in the payload to discount the line item. 

Show the previous amount, the new amount and the amount discounted. 
Total will also need to be recalculated to account for the discount. 

Example below:
```
+-------------------------------------------------------------------------------+
| Invoice Time                                                                  |
| a project by test double                                   [Add New Invoice]  |
+-------------------------------------------------------------------------------+
| Home / Invoices / 6ba7b810-9dad-11d1-80b4-00c04fd430c8                        |
+-------------------------------------------------------------------------------+
|                                                                               |
| +------------+  +-----------------------------------------------------------+ |
| | CircleCi   |  |                   New Relic Invoice                       | |
| |            |  |                                                           | |
| | New Relic  |  | Attn: Matan Matan@new-relic.com                           | |
| |            |  | Due Date: 10/20/2020                                      | |
| |            |  | Notes: Send weekly invoices.                              | |
| |            |  |                                                           | |
| |            |  | Projects                                                  | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  | | Name          Description         Price    Qty  Total | | |
| |            |  | | Video Software Subscription...    $140      1   $140  | | |
| |            |  | |   Previous Amount: $140                               | | |
| |            |  | |   Discount (10%): -$14                                | | |
| |            |  | |   New Amount: $126                                    | | |
| |            |  | | Heroku        Monthly server...   $280      1   $280  | | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  |                                                           | |
| |            |  | Services                                                  | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  | | Name              Description     Price    Qty  Total | | |
| |            |  | | Software Dev.     Refactor...     $150      8   $1200 | | |
| |            |  | | Software Dev.     Extract...      $150     12   $1800 | | |
| |            |  | | Software Dev.     Deploy...       $150      9   $1350 | | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  |                                                           | |
| |            |  | Total: $4,756                                             | |
| |            |  |                                                           | |
| +------------+  +-----------------------------------------------------------+ |
|                                                                               |
+-------------------------------------------------------------------------------+
```

</details>

<details>
<summary>3: Add an "Invoice Complete" button</summary>

Add a button to the invoice to mark it as either complete or incomplete. 

If the invoice is "complete" the button should show "Mark as incomplete" and if the invoice is "incomplete" the button should show "Mark as complete."

Example below:

```
+-------------------------------------------------------------------------------+
| Invoice Time                                                                  |
| a project by test double                                   [Add New Invoice]  |
+-------------------------------------------------------------------------------+
| Home / Invoices / 6ba7b810-9dad-11d1-80b4-00c04fd430c8                        |
+-------------------------------------------------------------------------------+
|                                                                               |
| +------------+  +-----------------------------------------------------------+ |
| | CircleCi   |  |                   New Relic Invoice                       | |
| |            |  |                                                           | |
| | New Relic  |  | Attn: Matan Matan@new-relic.com                           | |
| |            |  | Due Date: 10/20/2020                                      | |
| |            |  | Notes: Send weekly invoices.                              | |
| |            |  |                                                           | |
| |            |  | [Mark as Complete] [Mark as Incomplete]                   | |
| |            |  |                                                           | |
| |            |  | Projects                                                  | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  | | Name          Description         Price    Qty  Total | | |
| |            |  | | Video Software Subscription...    $140      1   $140  | | |
| |            |  | | Heroku        Monthly server...   $280      1   $280  | | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  |                                                           | |
| |            |  | Services                                                  | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  | | Name              Description     Price    Qty  Total | | |
| |            |  | | Software Dev.     Refactor...     $150      8   $1200 | | |
| |            |  | | Software Dev.     Extract...      $150     12   $1800 | | |
| |            |  | | Software Dev.     Deploy...       $150      9   $1350 | | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  |                                                           | |
| |            |  | Total: $4,770                                             | |
| |            |  |                                                           | |
| +------------+  +-----------------------------------------------------------+ |
|                                                                               |
+-------------------------------------------------------------------------------+
```

</details>

<details>
<summary>4: Show Incomplete Count</summary>

Add a count of the number of invoices marked incomplete in the side navigation.

Example below:

```
+-------------------------------------------------------------------------------+
| Invoice Time                                                                  |
| a project by test double                                   [Add New Invoice]  |
+-------------------------------------------------------------------------------+
| Home / Invoices / 6ba7b810-9dad-11d1-80b4-00c04fd430c8                        |
+-------------------------------------------------------------------------------+
|                                                                               |
| +------------+  +-----------------------------------------------------------+ |
| | CircleCi   |  |                   New Relic Invoice                       | |
| |            |  |                                                           | |
| | New Relic  |  | Attn: Matan Matan@new-relic.com                           | |
| | (2 pending)|  | Due Date: 10/20/2020                                      | |
| |            |  | Notes: Send weekly invoices.                              | |
| |            |  |                                                           | |
| |            |  | [Mark as Complete]                                        | |
| |            |  |                                                           | |
| |            |  | Projects                                                  | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  | | Name          Description         Price    Qty  Total | | |
| |            |  | | Video Software Subscription...    $140      1   $140  | | |
| |            |  | | Heroku        Monthly server...   $280      1   $280  | | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  |                                                           | |
| |            |  | Services                                                  | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  | | Name              Description     Price    Qty  Total | | |
| |            |  | | Software Dev.     Refactor...     $150      8   $1200 | | |
| |            |  | | Software Dev.     Extract...      $150     12   $1800 | | |
| |            |  | | Software Dev.     Deploy...       $150      9   $1350 | | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  |                                                           | |
| |            |  | Total: $4,770                                             | |
| |            |  |                                                           | |
| +------------+  +-----------------------------------------------------------+ |
|                                                                               |
+-------------------------------------------------------------------------------+
```

</details>

<details>
<summary>5: Add Invoice States</summary>

The business has come back and asked that we add an additional state to the invoices. Now, instead of just "complete" or "incomplete", we need to mark invoices as: "Open", "Paid", or "Closed". 

And the sidebar should be changed to "# Open" instead of "# Incomplete".

Example below:

```
+-------------------------------------------------------------------------------+
| Invoice Time                                                                  |
| a project by test double                                   [Add New Invoice]  |
+-------------------------------------------------------------------------------+
| Home / Invoices / 6ba7b810-9dad-11d1-80b4-00c04fd430c8                        |
+-------------------------------------------------------------------------------+
|                                                                               |
| +------------+  +-----------------------------------------------------------+ |
| | CircleCi   |  |                   New Relic Invoice                       | |
| |            |  |                                                           | |
| | New Relic  |  | Attn: Matan Matan@new-relic.com                           | |
| | (2 open)   |  | Due Date: 10/20/2020                                      | |
| |            |  | Notes: Send weekly invoices.                              | |
| |            |  |                                                           | |
| |            |  | Status: [Open ▼]                                          | |
| |            |  |         • Open                                            | |
| |            |  |         • Paid                                            | |
| |            |  |         • Closed                                          | |
| |            |  |                                                           | |
| |            |  | Projects                                                  | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  | | Name          Description         Price    Qty  Total | | |
| |            |  | | Video Software Subscription...    $140      1   $140  | | |
| |            |  | | Heroku        Monthly server...   $280      1   $280  | | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  |                                                           | |
| |            |  | Services                                                  | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  | | Name              Description     Price    Qty  Total | | |
| |            |  | | Software Dev.     Refactor...     $150      8   $1200 | | |
| |            |  | | Software Dev.     Extract...      $150     12   $1800 | | |
| |            |  | | Software Dev.     Deploy...       $150      9   $1350 | | |
| |            |  | +-------------------------------------------------------+ | |
| |            |  |                                                           | |
| |            |  | Total: $4,770                                             | |
| |            |  |                                                           | |
| +------------+  +-----------------------------------------------------------+ |
|                                                                               |
+-------------------------------------------------------------------------------+
```

</details>
