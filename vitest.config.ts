/// <reference types="vitest" />
import { defineConfig } from "vitest/config";

export default defineConfig({
  test: {
    include: ["src/**/*.{test,spec}.{ts,tsx}"],
    includeSource: ["src/**/*.{ts,tsx}"],
    globals: true
  },
});